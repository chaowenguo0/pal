public class Sagemaker
{
    public static void main(final java.lang.String[] args) throws java.lang.Exception
    {
        final var userAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36";
        final var objectMapper = new com.fasterxml.jackson.databind.ObjectMapper();
        final var client = java.net.http.HttpClient.newHttpClient();
        final var login = objectMapper.readTree(client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create("https://gateway.prod.us-east-2.studiolab.sagemaker.aws/v1/signin")).header("user-agent", userAgent).POST(java.net.http.HttpRequest.BodyPublishers.ofString(objectMapper.writeValueAsString(java.util.Map.of("email", "chaowen.guo1@gmail.com", "password", args[0])))).build(), java.net.http.HttpResponse.BodyHandlers.ofString()).thenApply(java.net.http.HttpResponse::body).join());
        final var userEntityId = login.get("userEntityId").asText();
        final var idToken = login.get("idToken").asText();
        final var projects = objectMapper.readTree(client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create(java.lang.String.format("https://gateway.prod.us-east-2.studiolab.sagemaker.aws/v1/users/%s/projects", userEntityId))).headers("user-agent", userAgent, "authorization", "Bearer " + idToken).build(), java.net.http.HttpResponse.BodyHandlers.ofString()).thenApply(java.net.http.HttpResponse::body).join());
        final var projectId = projects.get("projectList").get(0).get("projectId").asText();
        java.util.concurrent.TimeUnit.SECONDS.sleep(15);
        final var appBuilder = java.net.http.HttpRequest.newBuilder(java.net.URI.create(java.lang.String.format("https://gateway.prod.us-east-2.studiolab.sagemaker.aws/v1/users/%s/projects/%s/app", userEntityId, projectId))).headers("user-agent", userAgent, "authorization", "Bearer " + idToken);
        client.send(appBuilder.DELETE().build(), java.net.http.HttpResponse.BodyHandlers.ofString());
        java.util.concurrent.TimeUnit.MINUTES.sleep(1);
        client.send(appBuilder.POST(java.net.http.HttpRequest.BodyPublishers.ofString(objectMapper.writeValueAsString(java.util.Map.of("userEntityId", userEntityId, "projectId", projectId, "runtime", "CPU")))).build(), java.net.http.HttpResponse.BodyHandlers.ofString());
        java.util.concurrent.TimeUnit.MINUTES.sleep(1);
        final var app = objectMapper.readTree(client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create(java.lang.String.format("https://gateway.prod.us-east-2.studiolab.sagemaker.aws/v1/users/%s/projects/%s/presigned-app-url", userEntityId, projectId))).headers("user-agent", userAgent, "authorization", "Bearer " + idToken).POST(java.net.http.HttpRequest.BodyPublishers.ofString(objectMapper.writeValueAsString(java.util.Map.of()))).build(), java.net.http.HttpResponse.BodyHandlers.ofString()).thenApply(java.net.http.HttpResponse::body).join());
        final var uri = java.net.URI.create(app.get("appUrl").asText());
        final var token = client.send(java.net.http.HttpRequest.newBuilder(uri).build(), java.net.http.HttpResponse.BodyHandlers.ofString()).headers().map().get("set-cookie").get(0).split(";")[0];
        final var pal = objectMapper.readTree(client.sendAsync(java.net.http.HttpRequest.newBuilder(new java.net.URI(uri.getScheme(), uri.getHost(), "/studiolab/default/jupyter/api/contents/pal.ipynb", null)).headers("cookie", token).build(), java.net.http.HttpResponse.BodyHandlers.ofString()).thenApply(java.net.http.HttpResponse::body).join());
        final var source = pal.get("content").get("cells").get(0).get("source").asText();
        final var session = objectMapper.readTree(client.sendAsync(java.net.http.HttpRequest.newBuilder(new java.net.URI(uri.getScheme(), uri.getHost(), "/studiolab/default/jupyter/api/sessions", null)).headers("cookie", "_xsrf=0;" + token, "X-XSRFToken", "0").POST(java.net.http.HttpRequest.BodyPublishers.ofString(objectMapper.writeValueAsString(java.util.Map.of("path", "", "type", "", "kernel", java.util.Map.of("name", "conda-env-default-py"))))).build(), java.net.http.HttpResponse.BodyHandlers.ofString()).thenApply(java.net.http.HttpResponse::body).join());
        java.lang.System.out.println(session);
        final var sessionId = session.get("id").asText();
        final var websocket = client.newWebSocketBuilder().buildAsync(new java.net.URI("wss", uri.getHost(), java.lang.String.format("/studiolab/default/jupyter/api/kernels/%s/channels?session_id=%s", session.get("kernel").get("id").asText(), sessionId), null), new java.net.http.WebSocket.Listener(){}).join();
        websocket.sendText(objectMapper.writeValueAsString(java.util.Map.of("buffers", java.util.List.of(), "channel", "shell", "content", java.util.Map.of("silent", false, "store_history", true, "user_expressions", java.util.Map.of(), "allow_stdin", true, "stop_on_error", true, "code", source), "header", java.util.Map.of("date", java.time.ZonedDateTime.now(java.time.ZoneId.of("UTC")).toString(), "msg_id", java.util.UUID.randomUUID(), "msg_type", "execute_request", "session", sessionId, "username", "" ,"version", "5.2"), "metadata", java.util.Map.of(), "parent_header", java.util.Map.of())), true);
        java.util.concurrent.TimeUnit.SECONDS.sleep(15);
    }
}