import * as playwright from 'playwright-chromium'
import path from 'path'
import process from 'process'
import {promises as fs} from 'fs'
import os from 'os'
import child_process from 'child_process'
import UserAgent from 'user-agents'

const context = await playwright.chromium.launchPersistentContext(await fs.mkdtemp(path.join(os.tmpdir(), 'pal')), {executablePath:'package/opt/google/chrome/google-chrome', args:['--disable-blink-features=AutomationControlled', '--start-maximized'], headless:false, recordVideo:{dir:'container'}, viewport:null})//default_args https://github.com/microsoft/playwright/blob/5faf6f9e69c2148e94c81675fb636eb31a02b5e7/src%2Fserver%2Fchromium%2Fchromium.ts#L78
const devcloud = await context.newPage()
await devcloud.goto('https://www.intel.com/devcloud-containers')
await devcloud.locator('input#signInName').fill('chaowen.guo1@gmail.com')
await devcloud.locator('button#continue').click()
await devcloud.locator('input#password').fill(process.argv.at(2))
await devcloud.locator('button#continue').click()
await devcloud.waitForURL('https://frontend.apps.cfa.devcloud.intel.com')
const token = await devcloud.context().cookies().then(_ => _.filter(_ => globalThis.Object.is(_.name, 'token'))).then(_ => _.at(0).value)
const projectId = 748305
await globalThis.fetch('https://frontend.apps.cfa.devcloud.intel.com/execution/api/v1/project/' + projectId, {method:'delete', headers:{authorization:`Bearer ${token}`, cookie:'XSRF-TOKEN=0', 'x-xsrf-token':0}})
await new globalThis.Promise(_ => globalThis.setTimeout(_, 1000 * 5))
globalThis.console.log(token, await globalThis.fetch('https://frontend.apps.cfa.devcloud.intel.com/byoc/api/v1/containers/deploy-multi-hardware', {method:'post', headers:{authorization:`Bearer ${token}`, cookie:'XSRF-TOKEN=0', 'x-xsrf-token':0, 'content-type':'application/json'}, body:globalThis.JSON.stringify([{projectId,edgeNodeId:1010}])}).then(_ => _.json()))
await devcloud.waitForTimeout(1000 * 20)
await context.close()

const username = 'chaowenguo_cbiyNg'
const password = 密码
const headers = {authorization:'Basic ' + globalThis.btoa(`${username}:${password}`)}
const buildId = await globalThis.fetch('https://api.browserstack.com/automate/builds.json', {headers}).then(_ => _.json()).then(_ => _?.at(0)?.automation_build?.hashed_id)
if (buildId) await globalThis.fetch('https://api.browserstack.com/automate/builds?buildId=' + buildId, {method:'delete', headers})

process.on('uncaughtException', _ => _)

import * as playwright from 'playwright-chromium'
import process from 'process'
import path from 'path'
import os from 'os'
import {promises as fs} from 'fs'
import * as tlsclient from '@dryft/tlsclient'
import tough from 'tough-cookie'
import jsdom from 'jsdom'
import child_process from 'child_process'
import AdmZip from 'adm-zip'

const cookiejar = new tough.CookieJar()
await cookiejar.setCookie(new tough.Cookie({key:'jwt', value:'4yj6g9ex5hhr5aonk2rteduiv08qnou1tb603sk9d8', domain:'loot.tv'}), 'https://loot.tv')
const axios = tlsclient.createTLSClient({validateStatus:false, timeout:2 * 60 * 1000, cookiejar})
new AdmZip(globalThis.Buffer.from(await axios.get('https://www.browserstack.com/browserstack-local/BrowserStackLocal-linux-x64.zip').then(_ => _.data), 'binary')).extractAllTo('browserStackLocal')
await fs.chmod('browserStackLocal/BrowserStackLocal', 0o755)
child_process.spawn('browserStackLocal/BrowserStackLocal', ['--key', process.argv.at(2), '--force-local'])
await new globalThis.Promise(_ => globalThis.setTimeout(_, 1000 * 5))
const caps =
{
    browser:'chrome',
    os:'osx',
    os_version:'Ventura',
    args:['--start-maximized', '--disable-blink-features=AutomationControlled'],
    'browserstack.local':'true',
    'browserstack.username':'fancydaddy_k469Mx',
    'browserstack.accessKey':process.argv.at(2),
    'browserstack.idleTimeout':'300',
    'client.playwrightVersion':child_process.spawnSync('npx', ['playwright', '--version']).stdout.toString().trim().split(' ').at(-1) // Playwright version being used on your local project needs to be passed in this capability for BrowserStack to be able to map request and responses correctly
}
const browser = await playwright.chromium.connect({wsEndpoint:`wss://cdp.browserstack.com/playwright?caps=${globalThis.encodeURIComponent(globalThis.JSON.stringify(caps))}`})
const context = await browser.newContext({viewport:null})
context.setDefaultTimeout(0)
console.log(await context.request.get('https://httpbin.org/ip').then(_ => _.json()))
const response = await axios.get(new globalThis.URL(await axios.post('https://api.loot.tv/api/videos/recommended/homepage/get').then(_ => _.data.data.at(0).videoID), 'https://loot.tv/video/').toString())
const virtualConsole = new jsdom.VirtualConsole()
const user = globalThis.JSON.parse(new jsdom.JSDOM(response.data, {url:response.config.url, virtualConsole}).window.document.querySelector('script#__NEXT_DATA__').textContent).props.initialState.user
console.log(await axios.post('https://api.loot.tv/api/account/redeem/points', {placementID:user.linkedPlacements.at(0).placementID, points:globalThis.Math.floor(user.balance / 10) * 10}).then(_ => _.data))
const loot = await context.newPage()
await context.addCookies([globalThis.Object.fromEntries(globalThis.Object.entries(await cookiejar.getCookies('https://loot.tv').then(_ => _.at(0).toJSON())).map(_ => globalThis.Object.is(_.at(0), 'key') ? ['name', _.at(1)] : _))])
await loot.goto(response.config.url)
globalThis.setInterval(async () => await loot.title(), 1000 * 60)

for (const _ of ['gmx.com', 'mailfence.com', 'proton.me', 'hotmail.com', 'gmail.com'])
{
    browserstack(_)
    await new globalThis.Promise(_ => globalThis.setTimeout(_, 1000 * 60))
}

import * as webdriverio from 'webdriverio'

async function browserstackApp()
{
    const opts =
    {
        protocol:'https',
        hostname:'hub-cloud.browserstack.com',
        path:'/wd/hub',
        user:'fancydaddy_k469Mx',
        key:'6TMxzbW3SKayiFnq5BkZ',
        capabilities:
        {
	        device:'Google Pixel 7',
            os_version:'13.0',
            app:'bs://sample.app',
            autoGrantPermissions:true,
            'browserstack.idleTimeout':'300'
        }
    }
    const client = await webdriverio.remote(opts)
    await client.startActivity('com.android.chrome', 'com.google.android.apps.chrome.Main')
    /*await client.$('id=com.android.permissioncontroller:id/continue_button').click()
    await client.$('id=android:id/button1').click()
    await client.$('id=com.android.chrome:id/message_primary_button').click()
    const contexts = await client.getContexts()
    await client.switchContext(contexts.at(1))
    const country = await client.execute(() => globalThis.fetch('https://ipinfo.io/json').then(_ => _.json()).then(_ => _.country))
    await client.switchToWindow('CDwindow-1')*/
    //console.log(await client.getContexts())
    await client.switchContext('WEBVIEW_chrome')
    await client.setCookies({name:'jwt', value:'f72eacc8odm0pj8aes54hw99r1e5okh912ja1otog5ig', domain:'loot.tv'})
    await client.url('https://loot.tv')
    await new globalThis.Promise(_ => globalThis.setTimeout(_, 1000 * 60))
    /*await client.switchContext(contexts.at(0))
    await client.$('id=com.android.chrome:id/toolbar_right_button').click()*/
    /*await client.switchContext(contexts.at(1))
    await client.switchToWindw('CDwindow-3')
    await client.url('https://www.youtube.com/watch?v=MLmqJbuEVvs')
    if (!globalThis.Object.is(country, 'US'))
    {
        await client.$('div=Read more').click()
        await client.$('button[aria-label="Accept the use of cookies and other data for the purposes described"]').click()
    }
    globalThis.setInterval(async () =>
    {
	for (const _ of await client.getWindowHandles())
	{
            await client.switchToWindow(_)
            await new globalThis.Promise(_ => globalThis.setTimeout(_, 1000 * 15))
        }	
    }, 1000 * (opts.capabilities['browserstack.idleTimeout'] - 200))*/
}

async function lambdatest()
{
    const caps =
    {
        browserName:'Chrome',
        'LT:Options':
	{
            platform:'Windows 11',
            user:'chaowen.guo1',
            accessKey:'gFtHgRhVSZxvqIrSBKUaximDHVY5kIBOo79YA5YFczRX7HjKoi',
            idleTimeout:'1800'
        }
     } 
     const browser = await chro1mium.connect({wsEndpoint:`wss://cdp.lambdatest.com/playwright?capabilities=${globalThis.encodeURIComponent(globalThis.JSON.stringify(caps))}`})
     const context = await browser.newContext()
     const alexamaster = await context.newPage()
     const [popup] = await globalThis.Promise.all([alexamaster.waitForEvent('popup'), alexamaster.goto('https://www.alexamaster.net/ads/autosurf/179036')])
     await popup.bringToFront()
     globalThis.setInterval(async () => await alexamaster.content(), 1000 * 60 * 20)
}

await globalThis.Promise.allSettled([...globalThis.Array.from({length:5}, () => [browserstack(), browserstackApp()]).flat()])
