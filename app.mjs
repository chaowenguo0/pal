import process from 'process'
import ioredis from 'ioredis'

const subscription = '493d8adb-4bc5-4e29-9341-e44e8c71df19'
const location = globalThis.Object.freeze(['westeurope', 'northeurope', 'francecentral', 'japanwest', 'koreacentral', 'koreasouth', 'uaenorth', 'eastasia', 'southeastasia', 'brazilsouth'])

const redis = new ioredis.Redis({host:'immune-eagle-44408.upstash.io', password:process.argv.at(2), tls:{}})
const token = await globalThis.fetch(`https://login.microsoftonline.com/${await redis.hget(subscription, 'tenant')}/oauth2/token`, {method:'post', body:new globalThis.URLSearchParams({grant_type:'client_credentials', client_id:await redis.hget(subscription, 'id'), client_secret:await redis.hget(subscription, 'secret'), resource:'https://management.azure.com/'})}).then(_ => _.json()).then(_ => _.access_token)
await redis.quit()

async function app(num)
{
    const group = `https://management.azure.com/subscriptions/${subscription}/resourcegroups/app${num}?api-version=2021-04-01`
    if (globalThis.Object.is((await globalThis.fetch(group, {method:'head', headers:{authorization:`Bearer ${token}`}})).status, 204))
    {
        const response = await globalThis.fetch(group, {method:'delete',  headers:{authorization:`Bearer ${token}`}})
        if (globalThis.Object.is(response.status, 202))
        {
            const header = response.headers
            while (true)
            {
                await new globalThis.Promise(_ => globalThis.setTimeout(_, header.get('retry-after') * 1000))
                if (globalThis.Object.is(await globalThis.fetch(header.get('location'), {headers:{authorization:`Bearer ${token}`}}).then(_ => _.status), 200)) break
            }
        }
    }
    await globalThis.fetch(group, {method:'put', headers:{authorization:`Bearer ${token}`, 'content-type':'application/json'}, body:globalThis.JSON.stringify({location:location.at(num)})})
    await new globalThis.Promise(_ => globalThis.setTimeout(_, 2 * 60 * 1000))
    const serverfarms = await globalThis.fetch(`https://management.azure.com/subscriptions/${subscription}/resourceGroups/app${num}/providers/Microsoft.Web/serverfarms/app${num}?api-version=2023-12-01`, {method:'put', headers:{authorization:`Bearer ${token}`, 'content-type':'application/json'}, body:globalThis.JSON.stringify({location:location.at(num), sku:{name:'F1'}, properties:{reserved:true}})})
    const json = await serverfarms.json()
    console.log(serverfarms.status, json)
    console.log(await globalThis.fetch(`https://management.azure.com/subscriptions/${subscription}/resourceGroups/app${num}/providers/Microsoft.Web/sites/app${num}cp?api-version=2023-12-01`, {method:'put', headers:{authorization:`Bearer ${token}`, 'content-type':'application/json'}, body:globalThis.JSON.stringify({location:location.at(num), properties:{serverFarmId:json.id, siteConfig:{linuxFxVersion:'DOCKER|quay.io/chaowenguo/app:latest'/*, appSettings:[{name:'RP_EMAIL', value:'chaowen.guo1@gmai.com'}, {name:'RP_API_KEY', value:'9b23ebb9-2ee1-427f-a271-f6fdf536537b'}], appCommandLine*/}}})}).then(_ => _.json()))
    //await globalThis.fetch(`https://management.azure.com/subscriptions/${subscription}/resourceGroups/app${num}/providers/Microsoft.Web/sites/app${num}bp?api-version=2021-02-01`, {method:'put', headers:{authorization:`Bearer ${token}`, 'content-type':'application/json'}, body:globalThis.JSON.stringify({location:location.at(num), properties:{serverFarmId:json.id, siteConfig:{linuxFxVersion:`COMPOSE|${globalThis.btoa(await fs.readFile(path.join(path.dirname(url.fileURLToPath(import.meta.url)), 'app.yml')))}`}}})})
}

await global.Promise.all(globalThis.Array.from({length:location.length}, (_, index) => app(index)))
/*const group = `https://management.azure.com/subscriptions/${subscription}/resourcegroups/app?api-version=2021-04-01`
if (globalThis.Object.is((await globalThis.fetch(group, {method:'head', headers:{authorization:`Bearer ${token}`}})).status, 204)) process.exit(0)
await globalThis.fetch(group, {method:'put', headers:{authorization:`Bearer ${token}`, 'content-type':'application/json'}, body:globalThis.JSON.stringify({location:'westus3'})})
const customerId = (await globalThis.fetch(`https://management.azure.com/subscriptions/${subscription}/resourcegroups/app/providers/Microsoft.OperationalInsights/workspaces/appapp?api-version=2021-12-01-preview`, {method:'put', headers:{authorization:`Bearer ${token}`, 'content-type':'application/json'}, body:globalThis.JSON.stringify({location:'westus3'})}).then(_ => _.json())).properties.customerId
const sharedKey = (await globalThis.fetch(`https://management.azure.com/subscriptions/${subscription}/resourcegroups/app/providers/Microsoft.OperationalInsights/workspaces/appapp/sharedKeys?api-version=2020-08-01`, {method:'post', headers:{authorization:`Bearer ${token}`}}).then(_ => _.json())).primarySharedKey
const managedEnvironments = await globalThis.fetch(`https://management.azure.com/subscriptions/${subscription}/resourceGroups/app/providers/Microsoft.App/managedEnvironments/app?api-version=2022-03-01`, {method:'put', headers:{authorization:`Bearer ${token}`, 'content-type':'application/json'}, body:globalThis.JSON.stringify({location:'westus3',properties:{appLogsConfiguration:{destination:'log-analytics', logAnalyticsConfiguration:{customerId, sharedKey}}}})})
if (globalThis.Object.is(managedEnvironments.status, 201))
{
    while (true)
    {
        await new globalThis.Promise(_ => globalThis.setTimeout(_, managedEnvironments.headers.get('retry-after') * 1000))
        if (globalThis.Object.is((await globalThis.fetch(managedEnvironments.headers.get('azure-asyncOperation'), {headers:{authorization:`Bearer ${token}`}}).then(_ => _.json())).status, 'Succeeded')) break
    }
}
await globalThis.fetch(`https://management.azure.com/subscriptions/${subscription}/resourceGroups/app/providers/Microsoft.App/containerApps/app?api-version=2022-03-01`, {method:'put', headers:{authorization:`Bearer ${token}`, 'content-type':'application/json'}, body:globalThis.JSON.stringify({location:'westus3', properties:{managedEnvironmentId:`/subscriptions/${subscription}/resourceGroups/app/providers/Microsoft.App/managedEnvironments/app`, configuration:{ingress:{external:true, targetPort:80}}, template:{containers:[{image:'quay.io/chaowenguo/pal:latest', name:'app', resources:{cpu:'.25', memory:'.5Gi'}}]}}})})*/