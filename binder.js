import {chromium} from 'playwright-chromium'

const browser = await chromium.launch({channel:'chrome', args:['--disable-blink-features=AutomationControlled', '--start-maximized'], headless:false})
const binder = await browser.newPage({recordVideo:{dir:'videos'}, viewport:null})
await binder.goto('https://mybinder.org/v2/git/https%3A%2F%2Fbitbucket.org%2Fchaowenguo%2Fpal/HEAD')
await binder.dblclick('li[title^="Name: pal.ipynb"]', {timeout:0})
await binder.click('button[data-command="runmenu:restart-and-run-all"]')
await binder.click('button.jp-mod-accept')