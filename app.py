import aiohttp, asyncio, base64, pathlib, builtins, sys, argparse, aredis
parser = argparse.ArgumentParser()
parser.add_argument('redis')

subscription = 'bc64100e-dcaf-4e19-b6ca-46872d453f08'
location = ('westus', 'westus2', 'eastus', 'eastus2', 'centralus', 'southcentralus', 'canadacentral', 'australiaeast', 'australiasoutheast', 'uksouth')

async def app(session, token, num):
    group = f'https://management.azure.com/subscriptions/{subscription}/resourcegroups/app{num}?api-version=2021-04-01'
    async with session.head(group, headers={'authorization':f'Bearer {token}'}) as response:
        if response.status == 204:
            async with session.delete(group, headers={'authorization':f'Bearer {token}'}) as response:
                if response.status == 202:
                    header = response.headers
                    while True:
                        await asyncio.sleep(builtins.int(header.get('retry-after')))
                        async with session.get(header.get('location'), headers={'authorization':f'Bearer {token}'}) as _:
                            if _.status == 200: break
    async with session.put(group, headers={'authorization':f'Bearer {token}'}, json={'location':location[num]}) as _: pass
    await asyncio.sleep(60 * 2)
    async with session.put(f'https://management.azure.com/subscriptions/{subscription}/resourceGroups/app{num}/providers/Microsoft.Web/serverfarms/app{num}', params={'api-version':'2022-03-01'}, headers={'authorization':f'Bearer {token}'}, json={'location':location[num], 'sku':{'name':'F1'}, 'properties':{'reserved':True}}) as serverfarms:
        serverfarmsJson = await serverfarms.json()
        builtins.print(serverfarms.status, serverfarmsJson)
        async with session.put(f'https://management.azure.com/subscriptions/{subscription}/resourceGroups/app{num}/providers/Microsoft.Web/sites/app{num}ap', params={'api-version':'2022-03-01'}, headers={'authorization':f'Bearer {token}'}, json={'location':location[num], 'properties':{'serverFarmId':serverfarmsJson.get('id'), 'siteConfig':{'linuxFxVersion':'DOCKER|quay.io/chaowenguo/app:latest'}}}) as _: pass
        #async with session.put(f'https://management.azure.com/subscriptions/{subscription}/resourceGroups/app{num}/providers/Microsoft.Web/sites/app{num}ap?api-version=2021-02-01', headers={'authorization':f'Bearer {token}'}, json={'location':location[num], 'properties':{'serverFarmId':serverfarmsJson.get('id'), 'siteConfig':{'linuxFxVersion':f"COMPOSE|{base64.b64encode(pathlib.Path(__file__).resolve().parent.joinpath('app.yml').read_bytes()).decode()}"}}}) as _: pass
        #async with session.put(f'https://management.azure.com/subscriptions/{subscription}/resourceGroups/app{num}/providers/Microsoft.Web/sites/app{num}ap', params={'api-version':'2022-03-01'}, headers={'authorization':f'Bearer {token}'}, json={'location':location[num], 'properties':{'serverFarmId':serverfarmsJson.get('id'), 'siteConfig':{'linuxFxVersion':'DOCKER|quay.io/chaowenguo/app:latest', 'appSettings':[{'name':'alexamaster', 'value':'157701'}]}}}) as _: pass
    
async def main():
    redis = aredis.StrictRedis(host='immune-eagle-44408.upstash.io', password=parser.parse_args().redis, ssl=True)
    async with aiohttp.ClientSession() as session:
        async with session.post(f'https://login.microsoftonline.com/{(await redis.hget(subscription, "tenant")).decode()}/oauth2/token', data={'grant_type':'client_credentials', 'client_id':(await redis.hget(subscription, 'id')).decode(), 'client_secret':(await redis.hget(subscription, 'secret')).decode(), 'resource':'https://management.azure.com/'}) as response:
            token = (await response.json()).get('access_token')
            await asyncio.gather(*(sys.modules[__name__].app(session, token, num) for num in builtins.range(10)))
            '''group = f'https://management.azure.com/subscriptions/{subscription}/resourcegroups/app?api-version=2021-04-01'
            async with session.head(group, headers={'authorization':f'Bearer {token}'}) as response:
                if response.status == 204: return
            async with session.put(group, headers={'authorization':f'Bearer {token}'}, json={'location':'eastus'}) as _: pass
            await asyncio.sleep(60 * 2)
            async with session.put(f'https://management.azure.com/subscriptions/{subscription}/resourcegroups/app/providers/Microsoft.OperationalInsights/workspaces/appapp', params={'api-version':'2021-12-01-preview'}, headers={'authorization':f'Bearer {token}'}, json={'location':'eastus'}) as customerId, session.post(f'https://management.azure.com/subscriptions/{subscription}/resourcegroups/app/providers/Microsoft.OperationalInsights/workspaces/appapp/sharedKeys', params={'api-version':'2020-08-01'}, headers={'authorization':f'Bearer {token}'}) as primarySharedKey:
                async with session.put(f'https://management.azure.com/subscriptions/{subscription}/resourceGroups/app/providers/Microsoft.App/managedEnvironments/app', params={'api-version':'2022-03-01'}, headers={'authorization':f'Bearer {token}'}, json={'location':'eastus', 'properties':{'appLogsConfiguration':{'destination':'log-analytics', 'logAnalyticsConfiguration':{'customerId':(await customerId.json()).get('properties').get('customerId'), 'sharedKey':(await primarySharedKey.json()).get('primarySharedKey')}}}}) as managedEnvironments:
                    if managedEnvironments.status == 201:
                        while True:
                            await asyncio.sleep(builtins.int(managedEnvironments.headers.get('retry-after')))
                            async with session.get(managedEnvironments.headers.get('azure-asyncOperation'), headers={'authorization':f'Bearer {token}'}) as _:
                                if (await _.json()).get('status') == 'Succeeded': break
            async with session.put(f'https://management.azure.com/subscriptions/{subscription}/resourceGroups/app/providers/Microsoft.App/containerApps/app', params={'api-version':'2022-03-01'}, headers={'authorization':f'Bearer {token}'}, json={'location':'eastus', 'properties':{'managedEnvironmentId':f'/subscriptions/{subscription}/resourceGroups/app/providers/Microsoft.App/managedEnvironments/app', 'configuration':{'ingress':{'external':True, 'targetPort':80}}, 'template':{'containers':[{'image':'quay.io/chaowenguo/pal:latest', 'name':'app', 'resources':{'cpu':'.25', 'memory':'.5Gi'}}]}}}) as _: pass'''

asyncio.run(main())

#https://www.microsoftazuresponsorships.com/
#az login --service-principal -u ${{secrets.AZURE}} -p {{secrets.AZUREPASSWORD}} --tenant ${{secrets.TENANT}} #az ad sp create-for-rbac --role Contributor --scopes /subscriptions/bc64100e-dcaf-4e19-b6ca-46872d453f08
#readonly location=(westus westus2 eastus eastus2 centralus southcentralus canadacentral australiaeast australiasoutheast uksouth)
#for i in ${!location[@]}
#do
#    if `az group exists -n app$i`
#    then
#        az group delete -n app$i -y --no-wait
#    fi
#done
#for i in ${!location[@]}
#do
#    if `az group exists -n app$i`
#    then
#        az group wait --deleted -g app$i
#    fi          
#    az group create -n app$i -l ${location[i]}
#    az appservice plan create -n app$i -g app$i --sku F1 --is-linux
#    az webapp create -n app${i}ap -p app$i -g app$i -i quay.io/chaowenguo/app:lastest
#    az webapp create -g app$i -p app$i -n app${i}ap --multicontainer-config-type compose --multicontainer-config-file app.yml
#    az webapp config appsettings set -n app${i}ap -g app$i --settings alexamaster=157701
#done
#az group exists -n app0 --debug# to the rest api
#cloudflare
#export default
#{
#    async scheduled(event, env, ctx)
#    {
#        ctx.waitUntil(globalThis.Promise.all(globalThis.Array.from({length:10}, (_, index) => [`https://app${index}ap.azurewebsites.net`, `https://app${index}cp.azurewebsites.net`].map(_ => globalThis.fetch(_))).flat()))
#    }
#}