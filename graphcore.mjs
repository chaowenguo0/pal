import * as playwright from 'playwright-chromium'
import child_process from 'child_process'
import path from 'path'
import process from 'process'
import {promises as fs} from 'fs'
import os from 'os'

const context = await playwright.chromium.launchPersistentContext(await fs.mkdtemp(path.join(os.tmpdir(), 'pal')), {channel:'chrome', args:['--disable-blink-features=AutomationControlled', '--start-maximized'], headless:false, recordVideo:{dir:'videos'}, viewport:null})//default_args https://github.com/microsoft/playwright/blob/5faf6f9e69c2148e94c81675fb636eb31a02b5e7/src%2Fserver%2Fchromium%2Fchromium.ts#L78
context.setDefaultTimeout(0)
const directory = path.dirname(new globalThis.URL(import.meta.url).pathname)
child_process.spawn(path.join(directory, 'Cli'), ['start', 'accept', '--token', 'ELGPy/DEQYDtARslA6HnkrbPIF6JQi+qYLCre5LBe58='])
/*const signIn = 'https://adfreeway.com/users/sign_in'
const adfreeway = await context.newPage()
await adfreeway.goto(signIn)
await adfreeway.request.post(signIn, {form:{'authenticity_token':await adfreeway.locator('input[name="authenticity_token"]').getAttribute('value'), 'user[email]':'kenneth.hassg@outlook.com', 'user[password]':process.argv.at(2)}})
await adfreeway.goto('https://adfreeway.com/myaccount')
await adfreeway.screenshot({path:'adfreeway.png', fullPage:true})
await fs.unlink(await adfreeway.video().path())*/
/*const lootup = await context.newPage()
await lootup.goto('https://lootup.me/login.php')
await lootup.waitForFunction(() => 'grecaptcha' in globalThis && 'execute' in globalThis.grecaptcha)
console.log(await lootup.request.post('https://lootup.me/api/user.php', {form:{recaptchaToken:await lootup.evaluateHandle(async () => await grecaptcha.execute('6Lezp5QUAAAAAKGU8YkPongMe1IJ9hm_GF04hejN', {action:'login'})).then(_ => _.jsonValue()), next:'earn.php', username:'kenneth.hagss@outlook.com', password:process.argv.at(2), method:'login', uid:null}}).then(_ => _.json()))
console.log(await lootup.request.post('https://lootup.me/api/user.php', {form:{method:'spinTheWheel'}}).then(_ => _.json()))
await lootup.close()
const hideout = await context.newPage()
await hideout.goto('https://hideout.co/login.php')
console.log(await hideout.request.post('https://hideout.co/api/user.php', {form:{recaptchaToken:await hideout.evaluateHandle(async () => await grecaptcha.execute('6LeLPnQUAAAAAG3MvaF-sih46uCiF3Em7r8jPANL', {action:'login'})).then(_ => _.jsonValue()), username:'kenneth.hagss@outlook.com', password:process.argv.at(2), method:'login', uid:null}}).then(_ => _.json()))
await hideout.goto('https://hideout.co/index.php')
await hideout.goto('https://hideout.co' + await hideout.locator('div#fragment-pane').locator('a').first().getAttribute('href'))
await hideout.locator('video[src]').evaluateHandle(_ => _.play())
console.log(await hideout.request.post('https://hideout.co/api/user.php', {form:{method:'redeemCode', code:await hideout.locator('input.promo-input-box').getAttribute('value')}}).then(_ => _.json()))
console.log(await hideout.request.post('https://hideout.co/api/user.php', {form:{method:'redeem', pubid:115588, adwall_id:17863, sub1:0, sub2:'', sub3:''}}).then(_ => _.json()))*/
const loot = await context.newPage()
await loot.route('https://client.px-cloud.net/PXxzao2rFc/main.min.js', _ => _.abort())
await loot.goto('https://loot.tv/account/login')
await loot.waitForFunction(() => 'grecaptcha' in globalThis && 'execute' in globalThis.grecaptcha)
let cookie = null
while(!(cookie = await globalThis.fetch('https://api.loot.tv/api/account/login', {method:'post', headers:{'content-type':'application/json'}, body:globalThis.JSON.stringify({recaptchaToken:await loot.evaluateHandle(async () => await grecaptcha.execute('6Lc6-FAfAAAAAElXjhH6DD2IzyNf46lL7hpXOe5-', {action:'login'})).then(_ => _.jsonValue()), email:'chaowen.guo1@gmail.com', password:process.argv.at(2)})}).then(_ => _.json()).then(_ => _.data)));
await context.addCookies([{name:globalThis.Object.keys(cookie).at(0), value:globalThis.Object.values(cookie).at(0), domain:'.loot.tv', path:'/'}])
await loot.goto('https://loot.tv')
const user = globalThis.JSON.parse(await loot.locator('script#__NEXT_DATA__').textContent()).props.initialState.user
await loot.goto('https://loot.tv' + await loot.locator('a[class*=SidenavItem_sidenavItem__]').nth(3).getAttribute('href'))
console.log(await globalThis.fetch('https://api.loot.tv/api/account/redeem/points', {method:'post', headers:{...cookie, 'content-type':'application/json'}, body:globalThis.JSON.stringify({placementID:user.linkedPlacements.at(0).placementID, points:globalThis.Math.floor(user.balance / 10) * 10})}).then(_ => _.json()))
const facebook = await context.newPage()
await facebook.goto('https://fb.watch/m8rnymcUwz')
await facebook.locator('div#scrollview+div').evaluateHandle(_ => _.remove())
const dailymotion = await context.newPage()
await dailymotion.goto('https://www.dailymotion.com/video/x87ytjz')
const rumble = await context.newPage()
await rumble.goto('https://rumble.com/user/chaowenguo1');
for await (const _ of await rumble.locator('a.video-item--a').all().then(_ => _.map(_ => _.getAttribute('href'))))
{
    await rumble.goto('https://rumble.com' + _)
    await rumble.locator('div#videoPlayer').click()
    await rumble.waitForTimeout(1000 * 60 * 3)
    const [minute, second] = await rumble.locator('div[title="Playback settings"]~div').last().locator('span').textContent().then(_ => _.split('/').at(1).split(':').map(globalThis.Number))
    await rumble.waitForTimeout(1000 * (minute * 60 + second + 10))
}
