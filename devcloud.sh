node=20.8.1
awk /inteloneapi/{\$0=\""#"\"\$0}1 ~/.bash_profile > /tmp/bash_profile
mv /tmp/bash_profile ~/.bash_profile
cat <<EOF >> ~/.bash_profile

export password=HL798820y+
export PATH=~/node-v$node-linux-x64/bin:~/package/usr/bin:\$PATH
export LD_LIBRARY_PATH=~/package/usr/lib/x86_64-linux-gnu
export PYTHONPATH=~/package/usr/lib/python3/dist-packages
export AWKPATH=~/package/usr/share/awk
export AWKLIBPATH=~/package/usr/lib/x86_64-linux-gnu/gawk
export JAVA_HOME=~/package/usr/lib/jvm/java-17-openjdk-amd64
export PERL5LIB=~/package/usr/share/perl5:~/package/usr/lib/x86_64-linux-gnu/perl5/5.30
EOF

. ~/.bash_profile
curl https://nodejs.org/dist/v$node/node-v$node-linux-x64.tar.xz | tar -xJ
curl -O https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
dpkg -x google-chrome-stable_current_amd64.deb package
apt download libnss3 libnspr4 libgbm1 libwayland-server0 xvfb x11-xkb-utils libxkbfile1 libxfont2 libfontenc1 python3-aiohttp python3-multidict python3-yarl python3-async-timeout python3-attr python3-bs4 python3-soupsieve python3-lxml fonts-noto-cjk python3-pip python-pip-whl python3-setuptools python3-wheel gawk x2goserver libx2go-config-perl libconfig-simple-perl libx2go-log-perl libx2go-server-perl libx2go-server-db-perl libx2go-utils-perl libdbi-perl x2goserver-common ca-certificates-java openjdk-17-jre-headless openjdk-17-jdk-headless
for i in *.deb
do
    dpkg -x $i package
done
rm -rf *.deb
cat <<EOF | xkbcomp -xkm - default.xkm #https://unix.stackexchange.com/questions/314335/how-to-run-xvfb-without-root/315172#315172
xkb_keymap "default" {
    xkb_keycodes             { include "evdev+aliases(qwerty)" };
    xkb_types                { include "complete" };
    xkb_compatibility        { include "complete" };
    xkb_symbols              { include "pc+us+inet(evdev)" };
    xkb_geometry             { include "pc(pc105)" };
};
EOF
echo -n '/bin/cp ~/default.xkm /tmp/server-99.xkm                           ' | dd bs=1 of=~/package/usr/bin/Xvfb seek=$(grep -FobUa '"%s%sxkbcomp' ~/package/usr/bin/Xvfb | awk -F : {print\$1}) conv=notrunc
PLAYWRIGHT_SKIP_BROWSER_DOWNLOAD=1 npm install playwright-chromium lodash-es google-auth-library webdriverio
gawk -i inplace /const\ ffmpegExecutable/{sub\(/find.+$/\,\"\'/usr/bin/ffmpeg\'\;\"\)}1 $(npm root)/playwright-core/lib/server/registry/index.js
gawk -i inplace /^WHEEL_DIR/{gsub\(/prefix/\,\"os.path.expanduser\(\'~\'\)\,\ \'package/usr\'\"\)}1 ~/package/usr/lib/python3/dist-packages/pip/_vendor/__init__.py
link()
{
    for i in $1/*
    do
        if [[ -h $i ]]
        then
            ln -f ~/package$(ls -l $i | awk {print\$NF}) $i
        elif [[ -d $i ]]
        then
            link $(realpath $i)
        fi
    done
}
link $(realpath .)
curl https://dl.google.com/android/repository/commandlinetools-linux-9123335_latest.zip -o commandline.zip
unzip commandline.zip
rm -rf commandline.zip
mv cmdline-tools latest
mkdir -p sdk/cmdline-tools
mv latest sdk/cmdline-tools
echo y | sdk/cmdline-tools/latest/bin/sdkmanager --no_https system-images\;android-30\;google_apis\;x86_64 platform-tools platforms\;android-33 --channel=0
echo no | sdk/cmdline-tools/latest/bin/avdmanager create avd -f -n android -k system-images\;android-30\;google_apis\;x86_64
sdk/emulator/emulator -avd android -no-window -gpu swiftshader_indirect -no-snapshot -noaudio -no-boot-anim -memory 4096
curl -O https://bitbucket.org/chaowenguo/pal/raw/main/devcloud.js
#qsub -I #then run bash setup.sh
#https://www.intel.com/content/www/us/en/developer/tools/devcloud/overview.html
#https://signin.intel.com/Identity?&amp;TARGET=https://registrationcenter.intel.com/AdminAddEditAccount.aspx?ChangeInfo=1#change email username
#https://www.intel.com/content/www/us/en/support/articles/000056108/services/warranty.html
#https://www.intel.com/content/www/us/en/secure/my-intel/support.html
#apkpure
