import aiohttp, asyncio, datetime, uuid, builtins, argparse, lxml.etree, urllib.request
parser = argparse.ArgumentParser()
parser.add_argument('password')
with urllib.request.urlopen('https://www.useragents.me') as _: userAgent = lxml.etree.parse(_, lxml.etree.HTMLParser()).xpath('//textarea[@class="form-control ua-textarea" and contains(text(), "Linux x86_64")]/text()')[0]

async def main():
    async with aiohttp.ClientSession(headers={'User-Agent':userAgent}) as client:
        async with client.post('https://api.paperspace.io/users/login', params={'include':'user'}, json={'email':'chaowen.guo1@gmail.com','password':parser.parse_args().password,'PS_REQUEST_VALIDATION_KEY':'Nu/CfHRkn2A1YqTQHNfzrWgIJF+iV/0B+QfTXDcya2g='}) as login:
            accessToken = (await login.json()).get('id')
            async with client.get('https://api.paperspace.io/notebooks/rtqxdnvdd9hlgt7/getNotebook', json={'namespace':'chaowenguo', 'access_token':accessToken}) as notebook:
                notebookId = (await notebook.json()).get('id')
                async with client.post('https://api.paperspace.io/notebooks/v2/stopNotebook', json={'access_token':accessToken, 'notebookId':notebookId, 'namespace':'chaowenguo'}) as _: pass
                await asyncio.sleep(60)
                while True:
                    async with client.post('https://api.paperspace.io/notebooks/v2/startNotebook', json={'access_token':accessToken,'notebookId':notebookId,'vmTypeLabel':'Free-GPU','teamId':1158853,'shutdownTimeout':6}) as machine:
                        if machine.status == 429:
                            print(await machine.json())
                            await asyncio.sleep(60)
                            continue
                        else:
                            print(machine.status)
                            _ = await machine.json()
                            token, fqdn = _.get('token'), _.get('fqdn')
                            print(fqdn)
                            while True:
                                async with client.get(f'https://{fqdn}/api/contents/pal.ipynb', headers={'authorization':f'token {token}'}) as pal:
                                    if pal.status != 200:
                                        print(pal.status, await pal.text())
                                        await asyncio.sleep(60)
                                        continue
                                    else:
                                        source = (await pal.json()).get('content').get('cells')[0].get('source')
                                        async with client.post(f'https://{fqdn}/api/sessions', headers={'authorization':f'token {token}'}, json={'path':'','type':''}) as session:
                                            _ = await session.json()
                                            kernel, sessionId = _.get('kernel').get('id'), _.get('id')
                                            print(_)
                                            async with client.ws_connect(f'wss://{fqdn}/api/kernels/{kernel}/channels', params={'session_id':sessionId}) as ws:
                                                 await ws.send_json({'buffers':[],'channel':'shell','content':{'silent':False,'store_history':True,'user_expressions':{},'allow_stdin':True,'stop_on_error':True,'code':source},'header':{'date':datetime.datetime.utcnow().isoformat(),'msg_id':builtins.str(uuid.uuid1()),'msg_type':'execute_request','session':sessionId,'username':'','version':'5.2'},'metadata':{},'parent_header':{}})
                                                 await asyncio.sleep(60)
                                        break
                            break

asyncio.run(main())                                                        
