rm -rf *
curl https://deb.nodesource.com/setup_current.x | bash -
apt update
curl https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb > chrome.deb
apt install -y --no-install-recommends nodejs xvfb xauth ./chrome.deb
rm -rf chrome.deb
curl https://bitbucket.org/chaowenguo/common/raw/main/package.json > package.json
export PLAYWRIGHT_SKIP_BROWSER_DOWNLOAD=1
npm install playwright-chromium
npx playwright install ffmpeg
curl https://bitbucket.org/chaowenguo/pal/raw/main/binder.js > binder.js
xvfb-run node binder.js