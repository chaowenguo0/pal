import * as playwright from 'playwright-chromium'
import process from 'process'
import path from 'path'
import {promises as fs} from 'fs'
import os from 'os'
import child_process from 'child_process'

async function page(context)
{
    while (true)
    {
        try
        {
            return await context.newPage()
        }
        catch {}
    }
}

const email = 'chaowen.guo1@gmail.com'
const directory = path.dirname(new globalThis.URL(import.meta.url).pathname)
function hg()
{
    return child_process.spawn(path.join(directory, 'hg'), ['-tou-accept', '-email', email, '-pass', process.argv.at(2), '-device', '2'], {env:{LD_LIBRARY_PATH:directory}})
}
function respawn(_)
{
    _.on('close', () => respawn(hg()))
}
respawn(hg())
child_process.spawn(path.join(directory, 'ipawns'), ['-accept-tos', '-email', email, '-password', process.argv.at(2), '-device-name', '2'])
const context = await playwright.chromium.launchPersistentContext(await fs.mkdtemp(path.join(os.tmpdir(), 'pal')), {executablePath:'package/opt/google/chrome/google-chrome', args:['--disable-blink-features=AutomationControlled', '--start-maximized'], headless:false, recordVideo:{dir:'devcloud'}, viewport:null})//default_args https://github.com/microsoft/playwright/blob/5faf6f9e69c2148e94c81675fb636eb31a02b5e7/src%2Fserver%2Fchromium%2Fchromium.ts#L78
context.setDefaultTimeout(0)
/*const lootup = await page(context)
await lootup.goto('https://lootup.me/login.php')
await lootup.waitForFunction(() => 'grecaptcha' in globalThis && 'execute' in globalThis.grecaptcha)
console.log(await lootup.request.post('https://lootup.me/api/user.php', {form:{recaptchaToken:await lootup.evaluateHandle(async () => await grecaptcha.execute('6Lezp5QUAAAAAKGU8YkPongMe1IJ9hm_GF04hejN', {action:'login'})).then(_ => _.jsonValue()), next:'earn.php', username:'marissa.pinkham@hotmail.com', password:process.argv.at(2), method:'login', uid:null}}).then(_ => _.json()))
console.log(await lootup.request.post('https://lootup.me/api/user.php', {form:{method:'spinTheWheel'}}).then(_ => _.json()))
await fs.unlink(await lootup.video().path())
await lootup.close()
const hideout = await page(context)
await hideout.goto('https://hideout.co/login.php')
await hideout.waitForFunction(() => 'grecaptcha' in globalThis && 'execute' in globalThis.grecaptcha)
console.log(await hideout.request.post('https://hideout.co/api/user.php', {form:{recaptchaToken:await hideout.evaluateHandle(async () => await grecaptcha.execute('6LeLPnQUAAAAAG3MvaF-sih46uCiF3Em7r8jPANL', {action:'login'})).then(_ => _.jsonValue()), username:'marissa.pinkham@hotmail.com', password:process.argv.at(2), method:'login', uid:null}}).then(_ => _.json()))
await hideout.goto('https://hideout.co/index.php')
console.log(await hideout.goto('https://hideout.co' + await hideout.locator('div#fragment-pane').locator('a').first().getAttribute('href')).then(_ => _.url()))
await hideout.locator('video[src]').evaluateHandle(_ => _.play())
console.log(await hideout.request.post('https://hideout.co/api/user.php', {form:{method:'redeemCode', code:await hideout.locator('input.promo-input-box').getAttribute('value')}}).then(_ => _.json()))
console.log(await hideout.request.post('https://hideout.co/api/user.php', {form:{method:'redeem', pubid:115588, adwall_id:17863, sub1:0, sub2:'', sub3:''}}).then(_ => _.json()))*/
const loot = await page(context) //https://timebucks.com/publishers/index.php?pg=my_completions&type=all&content=19&page=3 //https://timebucks.com/publishers/index.php?pg=my_completions_archive&page=5&type=all&content=19
await loot.route('https://client.px-cloud.net/PXxzao2rFc/main.min.js', _ => _.abort())
await loot.goto('https://loot.tv/account/login')
await loot.waitForFunction(() => 'grecaptcha' in globalThis && 'execute' in globalThis.grecaptcha)
let cookie = null
while(!(cookie = await globalThis.fetch('https://api.loot.tv/api/account/login', {method:'post', headers:{'content-type':'application/json'}, body:globalThis.JSON.stringify({recaptchaToken:await loot.evaluateHandle(async () => await grecaptcha.execute('6Lc6-FAfAAAAAElXjhH6DD2IzyNf46lL7hpXOe5-', {action:'login'})).then(_ => _.jsonValue()), email:'chaowen.guo1@outlook.com', password:process.argv.at(2)})}).then(_ => _.json()).then(_ => _.data)));
await context.addCookies([{name:globalThis.Object.keys(cookie).at(0), value:globalThis.Object.values(cookie).at(0), domain:'.loot.tv', path:'/'}])
await loot.goto('https://loot.tv')
const user = globalThis.JSON.parse(await loot.locator('script#__NEXT_DATA__').textContent()).props.initialState.user
await loot.goto('https://loot.tv' + await loot.locator('a[class*=SidenavItem_sidenavItem__]').nth(3).getAttribute('href'))
console.log(await globalThis.fetch('https://api.loot.tv/api/account/redeem/points', {method:'post', headers:{...cookie, 'content-type':'application/json'}, body:globalThis.JSON.stringify({placementID:user.linkedPlacements.at(0).placementID, points:globalThis.Math.floor(user.balance / 10) * 10})}).then(_ => _.json()))
const facebook = await page(context)
await facebook.goto('https://fb.watch/m8rnymcUwz')
await facebook.locator('div#scrollview+div').evaluateHandle(_ => _.remove())
const dailymotion = await page(context)
await dailymotion.goto('https://www.dailymotion.com/video/x87ytjz')
const rumble = await page(context)
await rumble.goto('https://rumble.com/user/chaowenguo1')
for await (const _ of await rumble.locator('a.video-item--a').all().then(_ => _.map(_ => _.getAttribute('href'))))
{
    await rumble.goto('https://rumble.com' + _)
    await rumble.locator('div#videoPlayer').click()
    await rumble.waitForTimeout(1000 * 60 * 3)
    const [minute, second] = await rumble.locator('div[title="Playback settings"]~div').last().locator('span').textContent().then(_ => _.split('/').at(1).split(':').map(globalThis.Number))
    await rumble.waitForTimeout(1000 * (minute * 60 + second + 10))
}
//const alexamaster = await context.newPage()
//const [popup] = await globalThis.Promise.all([alexamaster.waitForEvent('popup'), alexamaster.goto(`https://www.alexamaster.net/ads/autosurf/${process.env.alexamaster}`)])
//await popup.bringToFront()
/*#python docker.py packetstream/psclient 92a8a16854f6559a17bfab5eeace483ea265509e176bdbf927c9a033cc0a9421 psdecode exit-node
#mv packetstream/* .
#rm -rf packetstream
#nohup ~/exit-node $(~/psdecode 1N5G)?client_version=20.202.1548 &
rm -rf devcloud devcloud.sh.* hideout*
python3 cashmining.py $password &
python3 adfreeway.py $password &
Xvfb :99 &
DISPLAY=:99 node devcloud.mjs $password
const hideoutFile = globalThis.Object.keys({hideout}).at(0) + new globalThis.Date().toUTCString()
await hideout.locator('span#ptscount').evaluateHandle(_ => new globalThis.MutationObserver(_ => globalThis.console.info(_.at(-1).addedNodes.item(0).nodeValue)).observe(_, {childList:true}))
hideout.on('console', async _ => {if (globalThis.Object.is(_.type(), 'info')) await fs.appendFile(hideoutFile, [await _.args().at(0).jsonValue(), new globalThis.Date().toUTCString(), '\n'].join(''))})*/
