curl https://deb.nodesource.com/setup_17.x | bash -
apt update
apt install -y --no-install-recommends chromium-browser xvfb nodejs
apt clean
export PLAYWRIGHT_SKIP_BROWSER_DOWNLOAD=1
curl https://bitbucket.org/chaowenguo/common/raw/main/package.json -o package.json
npm install playwright-chromium
curl https://bitbucket.org/chaowenguo/pal/raw/main/js/pal.js | awk \!/^child_process/{gsub\(/channel:\'chrome\'/\,\"$'executablePath:\'/usr/lib/chromium-browser/chromium-browser\''\"\)\;print} > pal.js
alexamaster=180120 xvfb-run node pal.js